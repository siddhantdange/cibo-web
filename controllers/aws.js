
//AWS = require('aws-sdk');
//AWS.config.region = 'us-west-2';
crypto = require('crypto');
Knox = require('knox');
var client = Knox.createClient(require('../config/aws_config.json'));
var url = 'https://s3-us-west-1.amazonaws.com/cibo-food-west/food/'

//aws.config.loadFromPath('./config/aws_config.json');

module.exports = {
  putImage : function(image_name, image_data, data_size, data_mimetype, callback_success, callback_error){
    // Generate date based folder prefix
    var datePrefix = (new Date()).toDateString('YYYY[/]MM');
    var key = crypto.randomBytes(10).toString('hex');
    var hashFilename = key + '-' + image_name;
    var pathToImage = '/food/' + hashFilename;
    var main_url = url + hashFilename;

    var headers = {
        'Content-Length': data_size,
        'Content-Type': data_mimetype,
        'x-amz-acl': 'public-read'
    };

    client.putBuffer(image_data, pathToImage, headers, function(err, response){
      if (err) {
        console.error('error streaming image: ', new Date(), err);
        return callback_error({'error' : err});
      }
      if (response.statusCode !== 200) {
        console.error('error streaming image: ', new Date(), err);
        return callback_error({'error' : err});
      }
      console.log('Amazon response statusCode: ', response.statusCode);
      console.log('Your file was uploaded');
      callback_success({'url' : main_url});
    });
  }
}
