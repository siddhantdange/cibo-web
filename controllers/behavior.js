
mongoose = require('mongoose');
busboy = require('connect-busboy');
aws = require('./aws');
Behavior = mongoose.model('Behavior');
Status = require('../models/Status');
User = mongoose.model('User');

module.exports = {
  add_dish_like : function(req, res){
    var dish_id = req.body.dish_id;
    var user_id = req.body.user_id;

    Behavior.create({
      type : "LIKE",
      for_type : "Dish",
      for_type_id : dish_id,
      user_id : user_id
    }, function(err, behavior){
      if(err){
        status = Status.STATUS_FAILED;
        message = "Could not like dish"

        res.json({
          "status" : status,
          "message" : message
        });
      } else{
        res.json({
          status : Status.STATUS_OK
        });
      }
    });
  },

  add_dish_dislike : function(req, res){
    var dish_id = req.body.dish_id;
    var user_id = req.body.user_id;

    Behavior.create({
      type : "DISLIKE",
      for_type : "Dish",
      for_type_id : dish_id,
      user_id : user_id
    }, function(err, behavior){
      if(err){
        status = Status.STATUS_FAILED;
        message = "Could not dislike dish"

        res.json({
          "status" : status,
          "message" : message
        });
      } else{
        res.json({
          status : Status.STATUS_OK
        });
      }
    });
  }

}
