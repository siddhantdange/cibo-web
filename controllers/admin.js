
express = require('express');
User = mongoose.model('User');
Dish = mongoose.model('Dish');
Restaurant = mongoose.model('Restaurant');
Status = require('../models/Status');


module.exports = {
  stats : function(req, res){
    Dish.find({}, function(err, dishes){
      var num_dishes = dishes.length;
      Restaurant.find({}, function(err, restaurants){
        var num_restaurants = restaurants.length;
        User.find({}, function(err, users){
          var num_users = users.length;
          res.render('stats', {num_dishes : num_dishes, num_restaurants : num_restaurants, num_users : num_users});
        })
      });
    });
  },

  manage_dishes : function(req, res){
    Dish.find({}, function(err, dishes){
      var id_to_dish_img = {}

      for(var i = 0; i < dishes.length; i++){
        id_to_dish_img[dishes[i]['_id']] = dishes[i]['image_url'];
      }

      res.render('manage_dishes', {dish_data_str : JSON.stringify(id_to_dish_img)});
    });
  },

  manage_dishes_delete : function(req, res){
    var dish_id = req.body.dish_id;

    Dish.remove({
      _id : dish_id
    }, function(err, obj){
      res.json({
        status : Status.STATUS_OK
      });
    });

  }
}
