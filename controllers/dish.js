
mongoose = require('mongoose');
busboy = require('connect-busboy');
aws = require('./aws');
Dish = mongoose.model('Dish');
Restaurant = mongoose.model('Restaurant');
Like = mongoose.model('Like');

module.exports = {

  get_dishes : function(req, res){
    Dish.find({'number' : { $gt: req.body.base_num, $lt: req.body.base_num + 25 }},
      function(err, dishes_found){
        if(err || !dishes_found || dishes_found.length == 0){
          console.log('err finding dishes for base ' + req.body.base_num + ', ' + err);
          res.json('err pulling dishes for base');
        } else{
          var filtered_dishes = [];
          for(var dish_idx in dishes_found){
            var dish = dishes_found[dish_idx];
            var new_dish = {
                price : dish['price'],
                image_url: dish['image_url'],
                name: dish['name'],
                restaurant: dish['restaurant'],
                type: dish['type'],
                id : dish['_id'],
                avg_rating : dish['avg_rating'],
                savory: dish['savory'],
                sweet: dish['sweet'],
                spicy: dish['spicy']
            }
            filtered_dishes.push(new_dish);
          }

          res.json(filtered_dishes);
        }
      });
  },

  like_action : function(req, res){
    Like.create(req.body, function(err, like_created){
      if(err){
       console.log(err);
      }

      Dish.findOne({_id : req.body.dish_id}, function(err, dish_found){
        if(err || !dish_found){
          console.log(err);
          res.json('error finding dish');
        } else{
          if(dish_found){
            dish_found.likes.push(like_created.id);
            dish_found.save(function(err, dish_saved){
              if(err){
                console.log(err);
                res.json('error updating dish');
              } else{
                User.findOne({_id : req.body.user_id}, function(err, user_found){
                  user_found.likes.push(like_created.id);
                  user_found.save(function(err, user_saved){
                    if(err){
                      console.log(err);
                      res.json('error saving user');
                    } else{
                      res.json('success');
                    }
                  });
                })
              }
            });
          }
        }
      });
    });
  },

  upload : function(req, res){
    Restaurant.find({},function(err, restaurants){
      if(err){
        res.json(err);
      } else{
        var names = [];
        for(index in restaurants){
          names.push(restaurants[index]['name']);
        }
        console.log(names);
        res.render('dish_upload', {uploadLink : 'upload_action', restaurants : names});
      }
    });
  },

  upload_action : function(req, res){
    req.form_data = {};
    var final_buffer, final_filename, final_encoding, final_mimetype;

    //upload image to S3
    buffer = require('buffer');
    req.pipe(req.busboy);
    req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      final_filename = filename;
      final_encoding = encoding;
      final_mimetype = mimetype
      // Create the initial array containing the stream's chunks
      var fileRead = [];

      file.on('data', function(chunk) {
        // Push chunks into the fileRead array
        fileRead.push(chunk);
      });

      file.on('error', function(err) {
        console.log('Error while buffering the stream: ', err);
      });

      file.on('end', function() {
        // Concat the chunks into a Buffer
        final_buffer = buffer.Buffer.concat(fileRead);
      });
    });

    req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
      if(val.toLowerCase().trim().length == 0){
        res.json('no blanks fields');
      }

      req.form_data[fieldname] = val.toLowerCase();
    });

    req.busboy.on('finish', function() {
        aws.putImage(final_filename, final_buffer, final_buffer.length, final_mimetype, function(success_data){
          req.form_data.image_url = success_data['url'];
          put_into_db();
        }, function(err_data){
          res.json(err_data['error']);
        });
    });

    var put_into_db = function(){
      Dish.find({}, function(err, dishes){
        var dish_count = dishes.length();
        req.form_data.number = dish_count + 1;
        Restaurant.findOne({
          name : req.form_data.restaurant
        }, function(err, restaurant){
          if(err){
            res.json('err1 ' + err);
          } else{
            if(!restaurant){
              Restaurant.create({
                name: req.form_data.restaurant
              }, function(err, rest1){
                req.form_data.restaurant = rest1.id;
                Dish.create(req.form_data, function(err, dish){
                  rest1.dishes.push(dish.id);
                  if(err){
                    res.json('err2 ' + err);
                  } else{
                    Restaurant.update({
                      _id : rest1._id
                    }, {dishes: rest1.dishes}, function(err, rest1){
                      if(err) res.json(err.message);
                      else res.json('Success!');
                    });
                  }
                });
              });
            } else{
              req.form_data.restaurant = restaurant.id;
                Dish.create(req.form_data, function(err, dish){
                  if(err){
                    res.json('err3 ' + err);
                  } else{
                    restaurant.dishes.push(dish.id);
                    Restaurant.update({
                      _id : restaurant.id
                    }, {dishes: restaurant.dishes}, function(err, restaurant){
                      if(err) res.json(err.message);
                      else res.json('Success!');
                    });
                  }
                });
            }
          }
        });
      });

    }
  }
}
