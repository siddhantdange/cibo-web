
var express = require('express');
var user_controller = require('../controllers/user');
var behavior_controller = require('../controllers/behavior');
var policy = require('../config/policy');

module.exports = function(app){
  app.post("/user/behavior/dish/like", policy.is_token_valid, behavior_controller.add_dish_like);
  app.post("/user/behavior/dish/dislike", policy.is_token_valid, behavior_controller.add_dish_dislike);
  app.post("/user/create", user_controller.create);
  app.post("/user/login", user_controller.login);

}


