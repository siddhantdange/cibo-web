express = require('express');
var restaurant_controller = require('../controllers/restaurant');

module.exports = function(app){
  app.get("/restaurant", restaurant_controller.load);
}

