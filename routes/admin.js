
var express = require('express');
var admin_controller = require('../controllers/admin');
var policy = require('../config/policy');

module.exports = function(app){
  app.get("/admin/stats", admin_controller.stats);
  app.get("/admin/manage_dishes", admin_controller.manage_dishes);
  app.post("/admin/manage_dishes/delete", admin_controller.manage_dishes_delete);
}

