
var express = require('express');
var dish_controller = require('../controllers/dish');
var policy = require('../config/policy');

module.exports = function(app){
  app.get("/dish/upload", dish_controller.upload);
  app.post("/dish/upload_action", dish_controller.upload_action);
  app.post("/dish/like_action", policy.is_token_valid, dish_controller.like_action);
  app.post("/dish/get_dishes", policy.is_token_valid, dish_controller.get_dishes);
}

