// module dependencies
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// set up the schema
var LikeSchema = new Schema({
    user_id : { type: String, required: true },
    dish_id : { type: String, required: true },
    type : { type: String, required: true },
    converted : { type: Boolean, required: false },
});

// before save function equivalent
LikeSchema.pre('save', function(next){
  var now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
})

LikeSchema.set('toObject', { getters: true });

mongoose.model('Like', LikeSchema)
