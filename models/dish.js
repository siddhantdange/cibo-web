// module dependencies
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// set up the schema
var DishSchema = new Schema({
    name : { type: String, required: true },
    restaurant : { type: String, required: true },
    price : { type: String, required: true },
    image_url : { type: String, required: true },
    spicy : { type: Number, required: false },
    sweet : { type: Number, required: false },
    savory : { type: Number, required: false },
    type : { type: Array, required: false },
    likes : { type: Array, required: false },
    avg_rating : { type: Number, required: false },
    number : { type: Number, required: true },
});

// before save function equivalent
DishSchema.pre('save', function(next){
  var now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
})

DishSchema.set('toObject', { getters: true });

mongoose.model('Dish', DishSchema)
