// module dependencies
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// set up the schema
var BehaviorSchema = new Schema({
    type : { type: String, required: true },
    for_type : { type: String, required: true },
    for_type_id : { type: String, required: true },
    user_id : { type: String, required: true },
});

// before save function equivalent
BehaviorSchema.pre('save', function(next){
  var now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
})

BehaviorSchema.set('toObject', { getters: true });

mongoose.model('Behavior', BehaviorSchema)
