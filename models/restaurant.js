// module dependencies
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// set up the schema
var RestaurantSchema = new Schema({
    name : { type: String, required: false },
    city : { type: String, required: false },
    lat : { type: String, required: false },
    lon : { type: String, required: false },
    type : { type: String, required: false },
    ethnicity : { type: Number, required: false },
    quality : { type: Number, required: false },
    dishes : { type: Array, required: false },
    visits : { type: Array, required: false },
});

// before save function equivalent
RestaurantSchema.pre('save', function(next){
  var now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
})

RestaurantSchema.set('toObject', { getters: true });

mongoose.model('Restaurant', RestaurantSchema)
